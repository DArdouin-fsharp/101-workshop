module Utils

open Rop

[<Measure>] type EUR

type MissionAssignement<[<Measure>]'u> = {
    Location : string
    Reward : int<'u>
    Succeeded : bool
}

type State = 
    | Active
    | Inactive
    | Banned

type Agent = {
    FirstName : string
    LastName : string 
    State : State
    Assignement : Option<MissionAssignement<EUR>>
}

let analyseAgent =
    printfn "Agent analysis in progress..."

let activateAgent c =
    match c.State with
    | Banned _ -> fail "Trying to activate a traitor ? You might be one aswell ! - SYSTEM AUTO-DESTRUCTION ACTIVATED \n... \n... BOOM!"
    | _ -> succeedWithLog {c with State = Active} "Proceeding agent activation ..."

let endMission result (successFun : Agent -> unit) =
    match result with
    | Failure f -> printfn "Mission failed ! Here's the recap :\n%s" f.Head
    | Success (agent, _) -> 
        printfn "Everything went as expected. HQ !"
        successFun agent

let rdm min max = System.Random().Next(min, max)
